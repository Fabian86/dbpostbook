//
//  AppCoordinator.swift
//  DBPostBook
//
//  Created by Fabian Mecklenburg on 13.11.19.
//  Copyright © 2019 Fabian Mecklenburg. All rights reserved.
//

import Foundation
import UIKit

class AppCoordinator {
    
    // MARK: - Properties
    let window: UIWindow?
    lazy var rootViewController: UINavigationController = {
        let viewController = AuthenticationBuilder.viewController()
        return UINavigationController(rootViewController: viewController)
    }()
    
    
    // MARK: - Life cycle
    init(window: UIWindow?) {
        self.window = window
    }
    
}

extension AppCoordinator {
    
    func start() {
        guard let window = window else {
            return
        }
        
        window.rootViewController = rootViewController
        window.makeKeyAndVisible()
    }
    
    static func showBasicErrorAlert(with message: String?, viewController: UIViewController?) {
        let alertController = UIAlertController(title: "Error", message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: "Close", style: .cancel, handler: nil)
        alertController.addAction(action)
        viewController?.present(alertController, animated: true, completion: nil)
    }
}
