//
//  PostsBuilder.swift
//  DBPostBook
//
//  Created by Fabian Mecklenburg on 13.11.19.
//  Copyright © 2019 Fabian Mecklenburg. All rights reserved.
//

import Foundation

struct PostsBuilder {
    
    static func viewController(with user: User) -> PostsViewController {
        
        var coordinator = PostCoordinator()
        let viewModel = PostsViewModel(coordinator: coordinator,
                                       user: user)
        
        let viewController = PostsViewController(viewModel: viewModel)
        coordinator.viewController = viewController
        return viewController
    }
}
