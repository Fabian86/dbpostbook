//
//  PostCoordinator.swift
//  DBPostBook
//
//  Created by Fabian Mecklenburg on 13.11.19.
//  Copyright © 2019 Fabian Mecklenburg. All rights reserved.
//

import Foundation

struct PostCoordinator {
    
    // MARK: - Properties
    weak var viewController: PostsViewController?
    
}

extension PostCoordinator {
    
    func showErrorAlert(with message: String?) {
        AppCoordinator.showBasicErrorAlert(with: message, viewController: viewController)
    }
}
