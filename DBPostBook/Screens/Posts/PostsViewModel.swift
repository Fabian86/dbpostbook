//
//  PostsViewModel.swift
//  DBPostBook
//
//  Created by Fabian Mecklenburg on 13.11.19.
//  Copyright © 2019 Fabian Mecklenburg. All rights reserved.
//

import Foundation

class PostsViewModel {
    
    // MARK: - Properties
    private let interactor: FetchPostsInteractorProtocol
    private let coordinator: PostCoordinator
    private var model: [Post]?
    private let user: User
    var isFavouriteFiltered = false
    
    // MARK: - Life cycle
    init(interactor: FetchPostsInteractorProtocol = FetchPostsInteractor(),
         coordinator: PostCoordinator,
         user: User) {
        self.interactor = interactor
        self.coordinator = coordinator
        self.user = user
    }
    
}

extension PostsViewModel {
    
    var userId: String {
        return String(user.id)
    }
    
    var dataSourcePosts: [Post] {
        if isFavouriteFiltered {
            return model?.filter { $0.isFavourite == isFavouriteFiltered } ?? []
        } else {
            return model ?? []
        }
    }
    
    var numberOfRows: Int {
        return dataSourcePosts.count
    }
    
    func itemAtIndexPath(with indexPath: IndexPath) -> Post? {
        guard indexPath.row < dataSourcePosts.count else {
            return nil
        }

        return dataSourcePosts[indexPath.row]
    }
}

extension PostsViewModel {
    
    func fetchData(with userId: String, completion: @escaping (Bool) -> ()) {
        interactor.fetchPosts(with: userId) { [weak self] result in
            switch result {
            case .success(let posts):
                self?.model = posts
                completion(true)
            case .failure(let error):
                self?.coordinator.showErrorAlert(with: error.message)
            }
        }
    }
    
    func updateModel(with post: Post, completion: @escaping (Bool) -> ()) {
        model = model?.map { value in
            if post.id == value.id {
                return post
            } else {
                return value
            }
        }
        
        completion(isFavouriteFiltered)
    }
    
    func didPressFilterButton(completion: @escaping (Bool) -> ()) {
        isFavouriteFiltered = !isFavouriteFiltered
        completion(true)
    }
}

