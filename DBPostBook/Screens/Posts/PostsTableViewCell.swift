//
//  PostsTableViewCell.swift
//  DBPostBook
//
//  Created by Fabian Mecklenburg on 14.11.19.
//  Copyright © 2019 Fabian Mecklenburg. All rights reserved.
//

import UIKit

private struct CellLayout {
    static let space: CGFloat = 16.0
    static let buttonSize: CGSize = CGSize(width: 40, height: 40)
    static let selectedImage = Images.star?.tint(with: .yellow)
    static let unselectedImage = Images.star?.tint(with: .lightGray)
}

protocol PostsTableViewCellDelegate: class {
    func favouriteButtonPressed(with post: Post)
}

class PostsTableViewCell: UITableViewCell {
    
    // MARK: - Properties
    weak var delegate: PostsTableViewCellDelegate?
    var post: Post? {
        didSet {
            updateData()
        }
    }
    

    // MARK: - UI
      private let titleLabel = UILabel()
      private let descriptionLabel = UILabel()
      private let favouritButton = UIButton()
    
    // MARK: - Life cycle
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        setupView()
        setupLayout()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}

extension PostsTableViewCell {
    
    func setupView() {
        
        titleLabel.numberOfLines = 0
        titleLabel.font = UIFont.boldSystemFont(ofSize: 16)
        contentView.addSubview(titleLabel)
        
        descriptionLabel.numberOfLines = 0
        contentView.addSubview(descriptionLabel)
        
        favouritButton.addTarget(self, action: #selector(didPressButton), for: .touchUpInside)
        contentView.addSubview(favouritButton)
        
    }
    
    func setupLayout() {
        
        titleLabel.snp.makeConstraints { make in
            make.top.leading.equalToSuperview().offset(Layout.space)
        }
        
        favouritButton.snp.makeConstraints { make in
            make.size.equalTo(CellLayout.buttonSize)
            make.top.equalTo(CellLayout.space)
            make.right.equalTo(-CellLayout.space)
            make.leading.equalTo(titleLabel.snp.trailing).offset(Layout.space)
        }
        
        descriptionLabel.snp.makeConstraints { make in
            make.leading.equalTo(titleLabel.snp.leading)
            make.trailing.equalTo(favouritButton.snp.leading)
            make.bottom.equalToSuperview().offset(-Layout.space)
            make.top.equalTo(titleLabel.snp.bottom).offset(Layout.space)
        }
        
    }
    
}

extension PostsTableViewCell {
    
    @objc func didPressButton() {
        post?.isFavourite = post?.isFavourite == false
        guard let post = self.post else {
            return
        }
        
        delegate?.favouriteButtonPressed(with: post)
    }
    
    func updateData() {
        let image = post?.isFavourite == true ? CellLayout.selectedImage : CellLayout.unselectedImage
        favouritButton.setBackgroundImage(image, for: .normal)
        titleLabel.text = post?.title
        descriptionLabel.text = post?.body
    }
}
