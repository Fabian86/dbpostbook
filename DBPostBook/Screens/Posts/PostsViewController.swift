//
//  PostsViewController.swift
//  DBPostBook
//
//  Created by Fabian Mecklenburg on 13.11.19.
//  Copyright © 2019 Fabian Mecklenburg. All rights reserved.
//

import UIKit

private struct Texts {
    static let showAll = "Show all"
    static let onlyFav = "Show only favourites"
    static let title = "Posts"
}


class PostsViewController: UIViewController {

    // MARK: - Properties
    private let viewModel: PostsViewModel
    private let cellIdentifier = String(describing: PostsTableViewCell.self)
    
    // UI
    private let tableView = UITableView()
    private let switchButton = UIButton(type: .custom)
    
    
    // MARK: - Life cycle
    init(viewModel: PostsViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        fetchData()
        setupView()
        setupLayout()
    }

}

//MARK: - UI
private extension PostsViewController {
    
    func setupView() {
        
        title = Texts.title
        view.backgroundColor = .white
        
        view.addSubview(tableView)
        tableView.tableFooterView = UIView()
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 80
        tableView.register(PostsTableViewCell.self, forCellReuseIdentifier: cellIdentifier)
        tableView.dataSource = self
        
        switchButton.setTitle(Texts.onlyFav, for: .normal)
        switchButton.backgroundColor = .blue
        switchButton.layer.cornerRadius = Layout.cornerRadius
        switchButton.addTarget(self, action: #selector(didPressButton), for: .touchUpInside)
        view.addSubview(switchButton)
        
    }
    
    func setupLayout() {
        
        tableView.snp.makeConstraints { make in
            make.top.leading.trailing.equalToSuperview()
        }

        switchButton.snp.makeConstraints { make in
            make.height.equalTo(Layout.buttonHeight)
            make.leading.equalToSuperview().offset(Layout.space)
            make.trailing.equalToSuperview().offset(-Layout.space)
            make.top.equalTo(tableView.snp.bottom).offset(Layout.space)
            make.bottom.equalTo(view.safeAreaLayoutGuide.snp.bottom).offset(-Layout.space)
        }
    }
}

//MARK: - Action
private extension PostsViewController {
    
    func fetchData() {
        let userId = viewModel.userId
        viewModel.fetchData(with: userId) { [weak self] success in
            if success {
                self?.tableView.reloadData()
            }
        }
    }
    
    @objc func didPressButton() {
        
        viewModel.didPressFilterButton { [weak self] success in
            if success {
                self?.tableView.reloadData()
                let buttonTitle = self?.viewModel.isFavouriteFiltered == true ? Texts.showAll : Texts.onlyFav
                self?.switchButton.setTitle(buttonTitle, for: .normal)
            }
        }
    
    }
    
}

//MARK: - TableView Data Source
extension PostsViewController: UITableViewDataSource {

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfRows
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let post = viewModel.itemAtIndexPath(with: indexPath)
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath)
        if let cell = cell as? PostsTableViewCell {
            cell.post = post
            cell.delegate = self
        }
        
        return cell
    }
}

extension PostsViewController: PostsTableViewCellDelegate {
    func favouriteButtonPressed(with post: Post) {
        viewModel.updateModel(with: post) { [weak self] filter in
            if filter {
                self?.tableView.reloadData()
            }
        }
    }
}

