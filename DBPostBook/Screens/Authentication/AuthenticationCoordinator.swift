//
//  AuthenticationCoordinator.swift
//  DBPostBook
//
//  Created by Fabian Mecklenburg on 13.11.19.
//  Copyright © 2019 Fabian Mecklenburg. All rights reserved.
//

import Foundation

class AuthenticationCoordinator {

    // MARK: - Properties
    weak var viewController: AuthenticationViewController?

}

extension AuthenticationCoordinator {
    
    func navigateToPostsViewController(with user: User) {
        let postsViewController = PostsBuilder.viewController(with: user)
        viewController?.show(postsViewController, sender: self)
    }
    
    func showErrorAlert(with message: String?) {
        AppCoordinator.showBasicErrorAlert(with: message, viewController: viewController)
    }

}
