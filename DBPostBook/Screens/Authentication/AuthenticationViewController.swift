//
//  AuthenticationViewController.swift
//  DBPostBook
//
//  Created by Fabian Mecklenburg on 13.11.19.
//  Copyright © 2019 Fabian Mecklenburg. All rights reserved.
//

import UIKit
import SnapKit

private struct Texts {
    static let title = "LogIn"
    static let description = "User ID"
    static let placeholder = "Please enter a User ID"
    static let error = "Error"
}

class AuthenticationViewController: UIViewController {
    
    // MARK: - Properties
    private let viewModel: AuthenticationViewModel
    
    // UI
    private let titleLabel = UILabel()
    private let idTextField = UITextField()
    private let logInButton = UIButton(type: .custom)
    
    // MARK: - Life cycle
    init(viewModel: AuthenticationViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupView()
        setupLayout()
    }
    
}

//MARK: - UI
private extension AuthenticationViewController {
    
    func setupView() {
        view.backgroundColor = .white
        title = Texts.title
        
        titleLabel.text = Texts.description
        view.addSubview(titleLabel)
        
        idTextField.placeholder = Texts.placeholder
        idTextField.delegate = self
        view.addSubview(idTextField)
        
        logInButton.setTitle(Texts.title, for: .normal)
        logInButton.backgroundColor = .blue
        logInButton.layer.cornerRadius = Layout.cornerRadius
        logInButton.addTarget(self, action: #selector(didPressButton), for: .touchUpInside)
        view.addSubview(logInButton)
        
    }
    
    func setupLayout() {
        
        idTextField.snp.makeConstraints { make in
            make.leading.equalToSuperview().offset(Layout.space)
            make.trailing.equalToSuperview().offset(-Layout.space)
            make.centerY.equalToSuperview()
        }
        
        titleLabel.snp.makeConstraints { make in
            make.leading.equalTo(idTextField.snp.leading)
            make.trailing.equalTo(idTextField.snp.trailing)
            make.bottom.equalTo(idTextField.snp.top).offset(-Layout.space)
        }
        
        logInButton.snp.makeConstraints { make in
            make.height.equalTo(Layout.buttonHeight)
            make.leading.equalTo(idTextField.snp.leading)
            make.trailing.equalTo(idTextField.snp.trailing)
            make.bottom.equalTo(view.safeAreaLayoutGuide.snp.bottom).offset(-Layout.space)
        }
    }
}

//MARK - Action
extension AuthenticationViewController {
    @objc func didPressButton() {
        viewModel.fetchData(with: idTextField.text ?? "")
    }
}


//MARK - Delegate
extension AuthenticationViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField.isFirstResponder {
            textField.resignFirstResponder()
        }
        return true
    }
    
}
