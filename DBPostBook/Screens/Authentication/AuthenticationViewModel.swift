//
//  AuthenticationViewModel.swift
//  DBPostBook
//
//  Created by Fabian Mecklenburg on 13.11.19.
//  Copyright © 2019 Fabian Mecklenburg. All rights reserved.
//

import Foundation

class AuthenticationViewModel {
    
    // MARK: - Properties
    private let interactor: AuthenticateInteractorProtocol
    private let coordinator: AuthenticationCoordinator
    
    // MARK: - Life cycle
    init(interactor: AuthenticateInteractorProtocol = AuthenticateInteractor(),
         coordinator: AuthenticationCoordinator) {
        self.interactor = interactor
        self.coordinator = coordinator
    }
}

extension AuthenticationViewModel {
    
    func fetchData(with userId: String) {
        interactor.authenticate(with: userId) { [weak self] result in
            switch result {
            case .success(let user):
                self?.coordinator.navigateToPostsViewController(with: user)
            case .failure(let error):
                self?.coordinator.showErrorAlert(with: error.message)
            }
        }
    }
}
