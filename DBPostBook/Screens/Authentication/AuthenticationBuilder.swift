//
//  AuthenticationBuilder.swift
//  DBPostBook
//
//  Created by Fabian Mecklenburg on 13.11.19.
//  Copyright © 2019 Fabian Mecklenburg. All rights reserved.
//

import Foundation

struct AuthenticationBuilder {
    
    static func viewController() -> AuthenticationViewController {
        
        let coordinator = AuthenticationCoordinator()
        let viewModel = AuthenticationViewModel(coordinator: coordinator)
        
        let viewController = AuthenticationViewController(viewModel: viewModel)
        coordinator.viewController = viewController
        return viewController
        
    }
}
