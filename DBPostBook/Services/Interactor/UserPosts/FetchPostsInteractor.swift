//
//  FetchPostsInteractor.swift
//  DBPostBook
//
//  Created by Fabian Mecklenburg on 13.11.19.
//  Copyright © 2019 Fabian Mecklenburg. All rights reserved.
//

import Foundation

struct FetchPostsInteractor: FetchPostsInteractorProtocol {
    func fetchPosts(with userId: String, completion: @escaping (Result<[Post], DBError>) -> ()) {
        
        let url = BaseInteractor.enpoint(interactorURL: .userPosts(userId))
        let parameters: [String: String] = ["userId": userId]
        BaseInteractor().get(url: url, parameters: parameters) { (response: Result<Data, DBError>) in
            switch response {
            case .success(let userData):
                do {
                    let posts = try Decode.data(with: userData, input: [Post].self)
                    completion(.success(posts))
                } catch {
                    completion(.failure(DBError(error: error)))
                }
            case .failure(let error):
                completion(.failure(DBError(error: error)))
            }
        }
    }
    
}
