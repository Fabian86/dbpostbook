//
//  LogInInteractorProtocol.swift
//  DBPostBook
//
//  Created by Fabian Mecklenburg on 13.11.19.
//  Copyright © 2019 Fabian Mecklenburg. All rights reserved.
//

import Foundation

protocol AuthenticateInteractorProtocol {
    func authenticate(with userId: String, completion: @escaping (Result<User, DBError>) -> ())
}

