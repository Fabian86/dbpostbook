//
//  AuthenticateInteractorProtocol.swift
//  DBPostBook
//
//  Created by Fabian Mecklenburg on 13.11.19.
//  Copyright © 2019 Fabian Mecklenburg. All rights reserved.
//

import Foundation

struct AuthenticateInteractor: AuthenticateInteractorProtocol {
    func authenticate(with userId: String, completion: @escaping (Result<User, DBError>) -> ()) {
        
        let url = BaseInteractor.endpoint(interactorURL: .authenticate(userId))
        BaseInteractor().get(url: url) { (response: Result<Data, DBError>) in
            switch response {
            case .success(let userData):
                do {
                    let user = try Decode.data(with: userData, input: User.self)
                    completion(.success(user))
                } catch {
                    completion(.failure(DBError(error: error)))
                }
            case .failure(let error):
                completion(.failure(DBError(error: error)))
            }
        }
    }
}


