//
//  BaseInteractorProtocol.swift
//  DBPostBook
//
//  Created by Fabian Mecklenburg on 13.11.19.
//  Copyright © 2019 Fabian Mecklenburg. All rights reserved.
//

import Foundation

protocol BaseInteractorProtocol {
    func get(url: String?, parameters: [String: String], completion: @escaping  (Result<Data, DBError>) -> ())
}
