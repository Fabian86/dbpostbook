//
//  BaseInteractor.swift
//  DBPostBook
//
//  Created by Fabian Mecklenburg on 13.11.19.
//  Copyright © 2019 Fabian Mecklenburg. All rights reserved.
//

import Foundation

enum InteractorURL {
    case authenticate(String)
    case userPosts(String)
}

private let baseUrl = "https://jsonplaceholder.typicode.com/"

struct BaseInteractor: BaseInteractorProtocol {
    
    func get(url: String?, parameters: [String: String] = [:], completion: @escaping  (Result<Data, DBError>) -> ()) {
        
        guard let url = url?.addURLParameters(with: parameters) else {
            completion(.failure(DBError()))
            return
        }
        
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        
        URLSession.shared.dataTask(with: url) { (data, _, error) in
            DispatchQueue.main.async {
                guard let data = data else {
                    completion(.failure(DBError(error: error)))
                    return
                }
                
                completion(.success(data))
            }
        }.resume()
    }
}

extension BaseInteractor {
    
    static func endpoint(interactorURL: InteractorURL) -> String? {
        var url = ""
        switch interactorURL {
        case .authenticate(let id):
            url = "users/" + id
        case .userPosts:
            url = "posts"
        }
        
        return baseUrl + url
    }
}

extension String {
    
    func addURLParameters(with parameters: [String: String]) -> URL? {
        
        guard !parameters.isEmpty else {
            return URL(string: self)
        }
        
        guard var urlComponents = URLComponents(string: self) else {
            return nil
        }
        urlComponents.queryItems = []
        for (key, value) in parameters {
           urlComponents.queryItems?.append(URLQueryItem(name: key, value: value))
        }
        
        return urlComponents.url
    }
}
