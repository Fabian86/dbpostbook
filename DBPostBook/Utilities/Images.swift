//
//  Images.swift
//  DBPostBook
//
//  Created by Fabian Mecklenburg on 14.11.19.
//  Copyright © 2019 Fabian Mecklenburg. All rights reserved.
//

import Foundation
import UIKit

struct Images {
    static let star = UIImage(named: "star")
}
