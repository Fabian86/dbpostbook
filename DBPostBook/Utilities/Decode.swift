//
//  Decode.swift
//  DBPostBook
//
//  Created by Fabian Mecklenburg on 13.11.19.
//  Copyright © 2019 Fabian Mecklenburg. All rights reserved.
//

import Foundation

struct Decode {

    static func data<Element: Decodable>(with data: Data, input: Element.Type) throws -> Element {
        let decoder = JSONDecoder()
        decoder.dateDecodingStrategy = .iso8601
        
        do {
            let result = try decoder.decode(input.self, from: data)
            return result
        } catch {
           throw error
        }
    }
}
