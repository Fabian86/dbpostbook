//
//  Image+Tint.swift
//  DBPostBook
//
//  Created by Fabian Mecklenburg on 14.11.19.
//  Copyright © 2019 Fabian Mecklenburg. All rights reserved.
//

import Foundation
import UIKit

extension UIImage {
    
    func tint(with fillColor: UIColor?) -> UIImage {
        guard let fillColor = fillColor else {
            return self
        }
        let image = withRenderingMode(.alwaysTemplate)
        UIGraphicsBeginImageContextWithOptions(size, false, scale)
        fillColor.set()
        image.draw(in: CGRect(origin: .zero, size: size))
        
        guard let imageColored = UIGraphicsGetImageFromCurrentImageContext() else {
            return self
        }
        
        UIGraphicsEndImageContext()
        return imageColored
    }
}


