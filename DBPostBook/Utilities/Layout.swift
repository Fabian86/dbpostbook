//
//  Layout.swift
//  DBPostBook
//
//  Created by Fabian Mecklenburg on 14.11.19.
//  Copyright © 2019 Fabian Mecklenburg. All rights reserved.
//

import Foundation
import UIKit

struct Layout {
    static let cornerRadius: CGFloat = 10.0
    static let buttonHeight: CGFloat = 48.0
    static let space: CGFloat = 16.0
}
