//
//  User.swift
//  DBPostBook
//
//  Created by Fabian Mecklenburg on 13.11.19.
//  Copyright © 2019 Fabian Mecklenburg. All rights reserved.
//

import Foundation

struct User: Decodable {
    let id: Int
    let name: String
    let email: String
}

extension User {
    enum CodingKeys: CodingKey {
        case id
        case name
        case email
    }
}
