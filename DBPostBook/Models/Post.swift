//
//  Post.swift
//  DBPostBook
//
//  Created by Fabian Mecklenburg on 13.11.19.
//  Copyright © 2019 Fabian Mecklenburg. All rights reserved.
//

import Foundation

struct Post: Decodable {
    let id: Int
    let userId: Int
    let title: String
    let body: String
    var isFavourite: Bool = false
}

extension Post {
    enum CodingKeys: CodingKey {
        case id
        case userId
        case title
        case body
    }
}
