//
//  DBError.swift
//  DBPostBook
//
//  Created by Fabian Mecklenburg on 13.11.19.
//  Copyright © 2019 Fabian Mecklenburg. All rights reserved.
//

import Foundation

class DBError: Error {
    
    // MARK: - Properties
    var errorCode: Int?
    var userInfo: [String: Any]? = ["message": "Something went wrong"]
    
    // MARK: - Life cycle
    init(error: Error? = nil) {
        if let message = error?.localizedDescription {
            userInfo?["message"] = message
        }
    }
}

extension DBError {
    
    var message: String? {
        return userInfo?["message"] as? String
    }
}
